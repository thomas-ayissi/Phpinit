# Phpinit
PHPinit is a handful of OOP models that can be easily integrated to your web-project for extended functionality. Now I will probably be beaten for calling this OOP, cause it doesn't follow the OOP standard per se. OOP often represent Objects from the real world - Or data members - whereas these models is more function/method based. Resource models if you like. 

&nbsp;

## Examples
You can see the models in action on [PHPinit.com](http://phpinit.com)

Below you will find a quick introduction to each individual tools.

&nbsp;

## Table of Content
1. [Base64](#base64)
2. [Client](#client)
3. [Connection](#connection)
4. [Crud](#crud)
5. [Crypto](#crypto)
6. [Curl](#curl)
7. [Dir](#dir)
8. [Image](#image)
9. [Login](#login)
10. [Mailer](#mailer)
11. [Random](#random)
12. [Sanitizer](#sanitizer)
13. [Sftp](#sftp)
14. [Singleton](#singleton)
15. [Time](#time)
16. [Tls](#tls)
17. [Umoji](#umoji)
18. [Upload](#upload)
19. [Validators](#validators)

&nbsp;

### Base64
URL friendly Base64 encode/decode methods.

&nbsp;

### Client
Get client IP, browser language and current URL.

&nbsp;

### Connection
An empty connection model. 

&nbsp;

### Crud
Create, Read, Update and Delete (database).

&nbsp;

### Crypto
Encrypt/decrypt strings, using AES.

&nbsp;

### Curl
Make Curl requests to your server. PUT/POST Json Encode/Decode requests.

&nbsp;

### Dir
Scan dir (ls) and recursively remove folders and content. 

&nbsp;

### Image
Create thumbnails with ease. 

&nbsp;

### Login
Easy login/logout integration. 

&nbsp;

### Mailer
Sends E-mails. 

&nbsp;

### Random
Create random strings. 

&nbsp;

### Sanitizer
Sanitize your strings. Be aware that this should not be used for sanitizing database values.

&nbsp;

### Sftp
Secure FTP. This requires an additional module to be installed (php-ssh2).

&nbsp;

### Singleton
An empty, ready to use Singleton. A Singleton will only be loaded once.

&nbsp;

### Time
Create timestamps. Also have a greeting method, which response according to the time of day.

&nbsp;

### Tls
(Re)connect with SSL/TLS (HTTPS).

&nbsp;

### Umoji
Unicode emojis.

&nbsp;

### Upload
Easy to integrate upload method. 

&nbsp;

### Validators
Validate common data, such as String, Integer, IP and URL.
